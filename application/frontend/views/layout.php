<!DOCTYPE html>
<html>
<head>
    <title>Real Estate Project</title>
    <meta charset="utf-8">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/common.css" />

    <!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/lt.ie9.css" />
        <script type="text/javascript" src="<?php echo base_url()?>js/pie.js" />
    <! -->
   
</head>
<body>
    <header id="fixed-top">

    </header>
    <section>
        <nav>
            <ul>
                <li><a href="#!/home">home</a></li>
                <li><a href="#!/index">index</a></li>
                <li><a href="#!/content">content</a></li>
            </ul>
        </nav>

        <div data-ng-controller="IndexController">
            <div data-ng-view></div>
        </div>

    </section>
    <footer id="fixed-bottom">

    </footer>
</body>
<script type="text/javascript">
    var baseUrl = "<?php echo base_url()?>";
</script>
<script data-main="js/bootstrap.js" src="<?php echo base_url()?>js/require.js"></script>
</html>