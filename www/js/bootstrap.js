var baseUrl = baseUrl || '';

requirejs.config({
    baseUrl: baseUrl + 'js',
    paths: {
    	lib : 'lib',
        jquery : 'lib/jquery',
        angular : 'lib/angular',
        modernizr : 'lib/modernizr',
        underscore : 'lib/underscore'
    },
    shim : {
    	'angular' : {exports : 'angular'},
    	'modernizr' : {exports : 'Modernizr'},
    	'underscore' : {exports : '_'}
    },
    priority: [
        "angular"
    ],
    urlArgs: 'v='+Math.random(),
    catchError : true
});

requirejs.onError = function(e) {
    if (e.requireType === 'timeout') {
        //should trigger timeout error
    } else {}
};

requirejs([
      'jquery'
    , 'modernizr'
    , 'angular'
    , 'application'
    , 'underscore'
    , 'controllers/index'
],
function($, Modernizr, angular, Application, _, controller){

	document = document || window.document;

	angular.element(document).ready(function(){
        angular.bootstrap(document, ['Application']);
	});

});