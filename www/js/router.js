define([
	  'application'
], function(Application) {

	Application.config([
          '$routeProvider'
        , '$locationProvider'
        ,
    function($routeProvider, $locationProvider) {

        $locationProvider.html5Mode(false).hashPrefix('!');

        $routeProvider
            .when('/', {templateUrl : 'welcome', controller : 'IndexController'})
            .when('/home',{redirectTo : '/'})
            .when('/index',{redirectTo : '/'})
            .when('/content',{templateUrl : 'welcome/content', controller : 'ContentController'});

	}]);

    return Application;
});