define([
    'application'
], function(Application) {

    Application.factory('registry', function(){

        var vars = [];
        return {
            register : function(name, value){
                vars[name] = value;
            },
            get : function(name) {
                return vars[name];
            }
        }

    });

    return Application;
});