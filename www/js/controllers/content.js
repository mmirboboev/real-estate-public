'use strict';

define([
      'application'
    , 'underscore'
], function (Application, _) {

    function ContentController($scope, $rootScope) {

        $scope.controller = 'Content';

    };

    ContentController.$inject = ['$scope', '$rootScope'];

    Application.controller('ContentController', ContentController);

    return Application;
});