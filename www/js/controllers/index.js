'use strict';

define([
      'application'
    , 'underscore'
    , 'controllers/content'
    , 'router'
], function (Application, _) {

    function IndexController($scope) {
        $scope.controller = 'Index';
    };

    IndexController.$inject = ['$scope'];

    Application.controller('IndexController', IndexController);

	return Application;
});